﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using GarageStory.Services;
using GarageStory.Exceptions;
using GarageStory.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace GarageStory.Tests
{
    [TestClass]
    public class GarageStoryTest
    {
        private static readonly int[] Garages1 = { 1, 7 }; // more secure garages
        private static readonly int[] Garages2 = { 2, 3, 4, 5, 6 };
        private static Mock<IGarageCleaningService> garageCleaningService = new Mock<IGarageCleaningService>();
        
        private GarageService garageService = new GarageService(garageCleaningService.Object);

        [TestMethod]
        public void WhenCarIsParkedThenGarageIsCleaned()
        {
            Car classicCar = new Car("Aston Martin", "DB4", 1958, true);

            int garageWhereCarIsParked = garageService.RegisterInGarage(classicCar);

            Assert.IsTrue(GarageWithinArrayOfGarages(garageWhereCarIsParked, Garages1));
        }

        [TestMethod]
        public void WhenCarIsNotClassicThenParkedToUsualGarage()
        {
            Car simpleCar = new Car("Toyota", "Prius", 2015, false);

            int garageWhereCarIsParked = garageService.RegisterInGarage(simpleCar);
            // then
            Assert.IsTrue(GarageWithinArrayOfGarages(garageWhereCarIsParked, Garages2));
        }

        [TestMethod]
        [ExpectedException(typeof(FreeGarageIsNotFoundException))]
        public void WhenAllGaragesHaveCarsThenExceptionIsThrown()
        {
            // given
            Car simpleCar = new Car("Toyota", "Prius", 2015, false);
            ParkCarsInAllGarages();
            // when
            garageService.RegisterInGarage(simpleCar);
        }

        private bool GarageWithinArrayOfGarages(int garageWhereCarIsParked, int[] garages)
        {
            return garages.Contains(garageWhereCarIsParked);
        }

        private void ParkCarsInAllGarages()
        {
            Dictionary<int, Car> busyGarages = new Dictionary<int, Car>();
            foreach (var increasedSecurityGarage in Garages1)
            {
                busyGarages.Add(increasedSecurityGarage, new Mock<Car>("Aston Martin", "DB4", 1958, true).Object);
            }

            foreach (var usualGarage in Garages2)
            {
                busyGarages.Add(usualGarage, new Mock<Car>("Toyota", "Prius", 2015, false).Object);
            }

            garageService.SetGaragesList(busyGarages);
        }
    }
}

namespace GarageStory.Models
{
    public class Car
    {
        private const string ClassicCarAbbreviation = "C";
        private const string SimpleCarAbbreviation = "S";

        public Car(string manufacturer, string model, int year, bool isClassic)
        {
            Manufacturer = manufacturer;
            Model = model;
            Year = year;
            IsClassic = isClassic;
        }

        public string Manufacturer { get; private set; }

        public string Model { get; private set; }

        public int Year { get; private set; }

        //is car classic or not?
        public bool IsClassic { get; private set; }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", Manufacturer, Model, Year, IsClassic ? ClassicCarAbbreviation : SimpleCarAbbreviation);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) 
            {
                return false;
            }

            Car car = (Car)obj;
            return IsClassic == car.IsClassic &&
                Manufacturer == car.Manufacturer &&
                Model == car.Model;
        }

        public override int GetHashCode()
        {
            return Year + 17 * Manufacturer?.GetHashCode() ?? 0 + 17 * Model?.GetHashCode() ?? 0;
        }
    }
}

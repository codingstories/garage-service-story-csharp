using GarageStory.Models;

namespace GarageStory
{
    public interface IGarageService
    {
        int RegisterInGarage(Car car);
    }
}

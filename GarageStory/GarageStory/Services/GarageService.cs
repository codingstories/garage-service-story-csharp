using System.Collections.Generic;
using GarageStory.Exceptions;
using GarageStory.Models;

namespace GarageStory.Services
{
    public class GarageService : IGarageService
    {
        private static readonly int[] SecureGarages = { 1, 7 };
        private static readonly int[] SimpleGarages = { 2, 3, 4, 5, 6 };

        private readonly IGarageCleaningService garageCleaningService;

        private Dictionary<int, Car> garagesWithCar = new Dictionary<int, Car>();

        public GarageService(IGarageCleaningService garageCleaningService)
        {
            this.garageCleaningService = garageCleaningService;
        }

        public void SetGaragesList(Dictionary<int, Car> garagesWithCar)
        {
            this.garagesWithCar = garagesWithCar;
        }

        public int RegisterInGarage(Car car)
        {
            int? garage;
            if (car.IsClassic)
            {
                garage = FindFreeGarageFrom(SecureGarages);
            }
            else
            {
                garage = FindFreeGarageFrom(SimpleGarages);
            }

            if (garage != null)
            {
                Clean(garage.Value);
                ParkCar(garage.Value, car);
                return garage.Value;
            }
            else
            {
                throw new FreeGarageIsNotFoundException();
            }
        }

        private void Clean(int garage)
        {
            garageCleaningService.Cleaning(garage);
        }

        private void ParkCar(int garage, Car car)
        {
            garagesWithCar.Add(garage, car);
        }

        private int? FindFreeGarageFrom(int[] garages)
        {
            foreach (var garage in garages)
            {
                if (IsGarageFree(garage)) 
                {
                    return garage;
                }
            }

            return null;
        }

        private bool IsGarageFree(int garage)
        {
            return !garagesWithCar.ContainsKey(garage);
        }
    }
}

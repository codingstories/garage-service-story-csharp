# Garage Service Story

**To read**: [https://refactoringstories.com/file-view/https:%2F%2Fgit.epam.com%2FRefactoring-Stories%2Fgarage-service-story-csharp]

## Story Outline
This story contains group of components for public garage software package.
While classes design and code organization looks not so bad, there're many issues
with names of classes, functions, fields and variables which become a problem to read and understand business logic.

## Story Organization
**Story Branch**: master
>`git checkout master`

**Practical task tag for self-study**:task
>`git checkout task`

Tags: #clean_code, #naming
